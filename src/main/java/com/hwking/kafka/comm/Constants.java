package com.hwking.kafka.comm;

/**
 * Created by wangqiwen on 2019/10/15.
 */
public class Constants {

    public final static int CODE_SUCCESS = 200; //操作成功
    public final static int CODE_FAIL = -1; //操作失败
    public final static int CODE_PARAMETERS_ERROR = -2; //参数错误

}
