package com.hwking.kafka.comm;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


public class Utils {



    /**
     * 判断变量是否为空
     *@author shiqx
     * @param s
     * @return
     */
    public static boolean isEmpty(String s) {
        if (null == s || "".equals(s) || "".equals(s.trim()) || "null".equalsIgnoreCase(s)|| "nullnull".equalsIgnoreCase(s)||"0".equals(s.trim())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 根据给定的Date对象，返回YYYYMMDDHH24MMSS格式字符串
     * @param date
     * @return
     */
    public static String formatDateFull(Date date){
        DateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
        return format1.format(date);

    }

    /**
     * 返回当前时间　格式：yyyy-MM-dd hh:mm:ss
     * @return String
     */
    public static String fromDateH(){
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format1.format(new Date());
    }
    /**
     * 返回当前时间　格式：yyyy-MM-dd
     * @return String
     */
    public static String fromDateY(){
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        return format1.format(new Date());
    }
    /**
     * 用来去掉List中空值和相同项的。
     *
     * @param list
     * @return
     */
    public static List<String> removeSameItem(List<String> list) {
        List<String> difList = new ArrayList<String>();
        for (String t : list) {
            if (t != null && !difList.contains(t)) {
                difList.add(t);
            }
        }
        return difList;
    }



    /**
     * uuid 32位
     * @return
     */
    public static String getUuid() {

        String uuid=UUID.randomUUID().toString().replaceAll("-","");
        return uuid;
    }

    /**
     * 校验是否为数字
     *
     * @param str
     * @return
     */
    public static boolean isNumber(String str) {
        byte[] chars = str.getBytes();
        for (byte b : chars) {
            if (b < '0' || b > '9') {
                return false;
            }
        }
        return true;
    }

    /**
     * 是否为英文
     * @param charaString
     * @return
     */
    public static boolean isEnglish(String charaString){

        return charaString.matches("^[a-zA-Z]*");

    }

    /**
     * 获取时间
     * @return
     */
    public static String getTime(){
        Date d = new Date();
        System.out.println(d);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        String dateNowStr = sdf.format(d);
        return dateNowStr.toString();
    }


    /**
     * 获取时间戳
     * @return
     */
    public static String currentTimeMillis(){
        String time =String.valueOf(System.currentTimeMillis());
        return time;
    }

    /**
     * 获取时间
     * @return
     */
    public static Date nowDate(){
        Date date=new Date();
        return date;
    }


    private static long tmpID = 0;

    private static boolean tmpIDlocked = false;

    /**
     *  获取long型 唯一id
     * @return
     */
    public static long getId()
    {
        long ltime = 0;
        while (true)
        {
            if(tmpIDlocked == false)
            {
                tmpIDlocked = true;
                //当前：（年、月、日、时、分、秒、毫秒）*10000
                ltime = Long.valueOf(new SimpleDateFormat("yyMMddhhmmssSSS").format(new Date()).toString()) * 10000;
                if(tmpID < ltime)
                {
                    tmpID = ltime;
                }
                else
                {
                    tmpID = tmpID + 1;
                    ltime = tmpID;
                }
                tmpIDlocked = false;
                return ltime;
            }
        }
    }

    /**
     * terminal_id 生成8位随机字符串，数字+英文字母
     * @return
     */
    public static String terminalId() {
        int maxNum = 36;
        int i;
        int count = 0;
        char[] str = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
                'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
        StringBuffer pwd = new StringBuffer("");
        Random r = new Random();
        while (count < 8) {
            i = Math.abs(r.nextInt(maxNum));
            if (i >= 0 && i < str.length) {
                pwd.append(str[i]);
                count++;
            }
        }
        return pwd.toString();
    }

    /***
     * 生成secret_key 8位数字
     */
    public static String secretKey(){
        Random random = new Random();
        String result="";
        for(int i=0;i<8;i++){
            //首字母不能为0
            result += (random.nextInt(9)+1);
        }
        return result;
    }

    public static void main(String[] args){
        long a=getId();
        System.out.println(a);
    }

}
