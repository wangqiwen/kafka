package com.hwking.kafka.bean;

/**
 *  发送类型
 */
public class ConfType {

    private String opt_type;

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }
}