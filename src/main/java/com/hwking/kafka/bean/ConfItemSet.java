package com.hwking.kafka.bean;

import java.util.List;


/**
 *  node节点设置项实体
 */
public class ConfItemSet {

    /**
     *  接收端的服务类型  DIR/MRC/OSD
     */
    private Integer receiver_service_type;
    /**
     * 终端节点 id
     */
    private String receiver_terminal_id;
    /**
     * 配置管理服务ID
     */
    private String issued_service_id;

    /**
     * 指令下发时间
     */
    private String issued_datetime;
    /**
     * 配置项
     */
    private Object param;

    /**
     *  序列号
     */
    private String serial_no;

    /**
     * 指令类型, conf_update
     */
    private String opt_type;

    public Integer getReceiver_service_type() {
        return receiver_service_type;
    }

    public void setReceiver_service_type(Integer receiver_service_type) {
        this.receiver_service_type = receiver_service_type;
    }

    public String getReceiver_terminal_id() {
        return receiver_terminal_id;
    }

    public void setReceiver_terminal_id(String receiver_terminal_id) {
        this.receiver_terminal_id = receiver_terminal_id;
    }

    public String getIssued_service_id() {
        return issued_service_id;
    }

    public void setIssued_service_id(String issued_service_id) {
        this.issued_service_id = issued_service_id;
    }

    public String getIssued_datetime() {
        return issued_datetime;
    }

    public void setIssued_datetime(String issued_datetime) {
        this.issued_datetime = issued_datetime;
    }

    public Object getParam() {
        return param;
    }

    public void setParam(Object param) {
        this.param = param;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }
}