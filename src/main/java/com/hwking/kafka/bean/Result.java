package com.hwking.kafka.bean;


/**
 * Created by wangqiwen on 2019/10/14.
 */
public class Result<T> {

    /**
     * 返回码
     */
    public int code;

    public T data;

    public Result(){

    }

    public Result(int code){
        this.code = code;
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }
}
