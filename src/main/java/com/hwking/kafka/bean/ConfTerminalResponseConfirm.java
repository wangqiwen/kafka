package com.hwking.kafka.bean;

/**
 * 激活确认
 */
public class ConfTerminalResponseConfirm {

    /**
     * 服务器响应时下发的序列号
     */
    private String serial_no;
    /**
     * // 指令类型, 注册响应确认  reg_resp_confirm
     */
    private String opt_type;
    /**
     * 终端id
     */
    private String terminal_id;
    /**
     * 确认时间
     */
    private String confirm_datetime;

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public String getConfirm_datetime() {
        return confirm_datetime;
    }

    public void setConfirm_datetime(String confirm_datetime) {
        this.confirm_datetime = confirm_datetime;
    }
}
