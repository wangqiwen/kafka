package com.hwking.kafka.bean;

import java.io.Serializable;
import java.util.Date;

public class MessageBean implements Serializable {
    /** uuid */
    private String uuid;

    /** 时间  */
    private Date date;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
