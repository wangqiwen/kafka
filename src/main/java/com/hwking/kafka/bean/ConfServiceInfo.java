package com.hwking.kafka.bean;

import java.util.Date;

/**
 * 服务器硬件信息 cpu,读写率,磁盘空间
 * {
 "terminal_id": "",
 "serial_no": "",
 "opt_type": "hw_status_report",
 "hw_status": {
 "cpu": {
 "percent": 1,
 "temperature": {
 "CPU 1": 32,
 "CPU 2": 63
 }
 },
 "memory": {
 "total": 123123,
 "available": 123123,
 "percent": 24,
 "used": 123,
 "free": 123
 },
 "disk_io": {
 "READ_IOPS": 123123,
 "WRITE_IOPS": 12123,
 "READ_SPEED_RATE": 123123,
 "WRITE_SPEED_RATE": 123123
 }
 },
 "report_datetime": "yyyy - MM - dd HH: mm: ss"
 }
 */

public class ConfServiceInfo {
    private String terminal_id;

    private String serial_no;

    private String opt_type;

    private String report_datetime;

    private Object hw_status;

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }

    public String getReport_datetime() {
        return report_datetime;
    }

    public void setReport_datetime(String report_datetime) {
        this.report_datetime = report_datetime;
    }

    public Object getHw_status() {
        return hw_status;
    }

    public void setHw_status(Object hw_status) {
        this.hw_status = hw_status;
    }

    public void setHw_status(String hw_status) {
        this.hw_status = hw_status;
    }
}