package com.hwking.kafka.bean;

public class ConfTerminal {

    /**
     * 发送方id
     */
    private String terminal_id;
    /**
     * 创建时有代码自动生成的8位数字 安全码， 用于终端激活
     */
    private String secret_key;
    /**
     *  配置节点IP地址，终端激活时上报
     */
    private String ip_address;
    /**
     * 创建时间
     */
    private String register_datetime;

    /**
     * 创建时间
     */
    private String opt_type;
    /**
     * uuid
     */
    private String dir_uuid;
    private String mrc_uuid;
    private String osd_uuid;

    public String getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(String terminal_id) {
        this.terminal_id = terminal_id;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public String getRegister_datetime() {
        return register_datetime;
    }

    public void setRegister_datetime(String register_datetime) {
        this.register_datetime = register_datetime;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }

    public String getDir_uuid() {
        return dir_uuid;
    }

    public void setDir_uuid(String dir_uuid) {
        this.dir_uuid = dir_uuid;
    }

    public String getMrc_uuid() {
        return mrc_uuid;
    }

    public void setMrc_uuid(String mrc_uuid) {
        this.mrc_uuid = mrc_uuid;
    }

    public String getOsd_uuid() {
        return osd_uuid;
    }

    public void setOsd_uuid(String osd_uuid) {
        this.osd_uuid = osd_uuid;
    }
}