package com.hwking.kafka.bean;

/**
 * 配置项 key-value
 */
public class ConfItem {

    private String conf_name;

    private String conf_value;

    public String getConf_name() {
        return conf_name;
    }

    public void setConf_name(String conf_name) {
        this.conf_name = conf_name;
    }

    public String getConf_value() {
        return conf_value;
    }

    public void setConf_value(String conf_value) {
        this.conf_value = conf_value;
    }
}