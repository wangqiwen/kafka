package com.hwking.kafka.bean;

/**
 * 服务端激活响应
 */
public class ConfTerminalResponse {

    /**
     * 下行指令发送服务ID
     */
    private String receiver_terminal_id;
    /**
     * 执行是否成功, SUCCESS / FAIL
     */
    private String execute_result;
    /**
     * 返回消息
     */
    private String message;
    /**
     * 服务端生成的随机序列号
     */
    private String serial_no;
    /**
     * 指令类型, 终端注册响应
     */
    private String opt_type;
    /**
     * 响应时间
     */
    private String response_datetime;

    public String getReceiver_terminal_id() {
        return receiver_terminal_id;
    }

    public void setReceiver_terminal_id(String receiver_terminal_id) {
        this.receiver_terminal_id = receiver_terminal_id;
    }

    public String getExecute_result() {
        return execute_result;
    }

    public void setExecute_result(String execute_result) {
        this.execute_result = execute_result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }

    public String getResponse_datetime() {
        return response_datetime;
    }

    public void setResponse_datetime(String response_datetime) {
        this.response_datetime = response_datetime;
    }
}