package com.hwking.kafka.bean;

/**
 *  配置信息下发响应
 */
public class ConfItemSetResponse {

    private String receiver_service_id;

    private String serial_no;

    private String execute_result;

    private String message;

    private String opt_type;

    private Object content;

    public String getReceiver_service_id() {
        return receiver_service_id;
    }

    public void setReceiver_service_id(String receiver_service_id) {
        this.receiver_service_id = receiver_service_id;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getExecute_result() {
        return execute_result;
    }

    public void setExecute_result(String execute_result) {
        this.execute_result = execute_result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getOpt_type() {
        return opt_type;
    }

    public void setOpt_type(String opt_type) {
        this.opt_type = opt_type;
    }
}