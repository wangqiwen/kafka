package com.hwking.kafka.service;

import com.hwking.kafka.bean.*;

public interface RestService {

    /**
     *  节点 首次激活
     *
     * @param onfTerminal
     * @return
     */
    public Result nodeActivate(ConfTerminal onfTerminal);

    /**
     *  节点 首次激活确认
     *
     * @param confTerminalResponseConfirm
     * @return
     */
    public Result nodeActivateConfirm(ConfTerminalResponseConfirm confTerminalResponseConfirm);

    /**
     *  节点 修改反馈
     *
     * @param confItemSetResponse
     * @return
     */
    public Result nodeConfUpdateConfirm(ConfItemSetResponse confItemSetResponse);

    /**
     *  硬件信息上报
     *
     * @param statusReportData
     * @return
     */
    public Result statusReport(String statusReportData);


}
