package com.hwking.kafka.service.impl;
import com.hwking.kafka.bean.*;
import com.hwking.kafka.comm.Constants;
import com.hwking.kafka.service.RestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;


@Service
public class RestServiceImpl implements RestService {


    private Logger logger = LoggerFactory.getLogger(RestServiceImpl.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${platform.url}")
    private String platformUrl;


    @Override
    public Result nodeActivate(ConfTerminal confTerminal) {
        Result<String> r = new Result();
        r.setCode(Constants.CODE_PARAMETERS_ERROR);
        String url = "http://" + platformUrl + "/osd/nodeActivate.htm";
        logger.info(" kafka nodeActivate  ： " + url);
        MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("terminal_id", confTerminal.getTerminal_id());
        requestEntity.add("secret_key", confTerminal.getSecret_key());
        requestEntity.add("ip_address", confTerminal.getIp_address());
        requestEntity.add("dir_uuid", confTerminal.getDir_uuid());
        requestEntity.add("mrc_uuid", confTerminal.getMrc_uuid());
        requestEntity.add("osd_uuid", confTerminal.getOsd_uuid());
        try {
            restTemplate.postForObject(url, requestEntity, String.class);
            r.setCode(Constants.CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public Result nodeActivateConfirm(ConfTerminalResponseConfirm confTerminalResponseConfirm) {
        Result<String> r = new Result();
        r.setCode(Constants.CODE_PARAMETERS_ERROR);
        String url = "http://" + platformUrl + "/osd/nodeActivateConfirm.htm";
        logger.info(" kafka nodeActivateConfirm  ： " + url);
        MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("terminal_id", confTerminalResponseConfirm.getTerminal_id());
        requestEntity.add("serial_no", confTerminalResponseConfirm.getSerial_no());
        requestEntity.add("opt_type", confTerminalResponseConfirm.getOpt_type());
        requestEntity.add("confirm_datetime", confTerminalResponseConfirm.getConfirm_datetime());
        try {
            restTemplate.postForObject(url, requestEntity, String.class);
            r.setCode(Constants.CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public Result nodeConfUpdateConfirm(ConfItemSetResponse confItemSetResponse) {
        Result<String> r = new Result();
        r.setCode(Constants.CODE_PARAMETERS_ERROR);
        String url = "http://" + platformUrl + "/osd/nodeConfUpdateResponse.htm";
        logger.info(" kafka nodeConfUpdateConfirm  ： " + url);
        logger.info(" kafka nodeConfUpdateConfirm execute_result ： " + confItemSetResponse.getExecute_result());

        MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("terminal_id", confItemSetResponse.getReceiver_service_id());
        requestEntity.add("execute_result", confItemSetResponse.getExecute_result());
        requestEntity.add("serial_no", confItemSetResponse.getSerial_no());
        requestEntity.add("opt_type", confItemSetResponse.getOpt_type());
        requestEntity.add("message", confItemSetResponse.getMessage());
        requestEntity.add("content", confItemSetResponse.getContent().toString());
        try {
            restTemplate.postForObject(url, requestEntity, String.class);
            r.setCode(Constants.CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }

    @Override
    public Result statusReport(String statusReportData) {
        Result<String> r = new Result();
        r.setCode(Constants.CODE_PARAMETERS_ERROR);
        String url = "http://" + platformUrl + "/osd/statusReport.htm";
        logger.info(" kafka statusReport  ： " + url);

        MultiValueMap<String, String> requestEntity = new LinkedMultiValueMap<>();
        requestEntity.add("statusReportData", statusReportData);
        try {
            restTemplate.postForObject(url, requestEntity, String.class);
            r.setCode(Constants.CODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }
}
