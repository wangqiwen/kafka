package com.hwking.kafka.controller;

import com.alibaba.fastjson.JSONObject;
import com.hwking.kafka.bean.*;
import com.hwking.kafka.comm.Constants;
import com.hwking.kafka.comm.Utils;
import com.hwking.kafka.listener.KafkaProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/kafka")
public class KafkaController {
    private Logger logger = LoggerFactory.getLogger(KafkaController.class);

        @Resource
        private KafkaProducer kafkaProducer;

/*    *//**
     * 节点注册激活 （节点--服务，此方法测试用）
     *
     * @return
     *//*
    @RequestMapping(value = "/nodeActivate", method = RequestMethod.POST)
    @ResponseBody
    public Result nodeActivate(String data) {
        Result result = new Result(Constants.CODE_PARAMETERS_ERROR);
        JSONObject jsStr = JSONObject.parseObject(data);
        logger.info("kafka nodeActivate info " + jsStr);
        ConfTerminal confTerminal = JSONObject.toJavaObject(jsStr, ConfTerminal.class);

        confTerminal.setTerminal_id("GWY2XW2I");
        confTerminal.setIp_address("192.168.31.199");
        confTerminal.setSecret_key("17732554");
        confTerminal.setOpt_type("terminal_register");
        confTerminal.setRegister_datetime(Utils.fromDateH());
        // 将消息发送到 kafka
        kafkaProducer.nodeActivate(confTerminal);
        // 返回成功信息
        result.setCode(Constants.CODE_SUCCESS);

        return result;
    }*/

    /**
     * 节点注册激活响应 (服务--节点)
     *
     * @return
     */
    @RequestMapping(value = "/nodeActivateResponse", method = RequestMethod.POST)
    @ResponseBody
    public Result nodeActivateResponse(ConfTerminalResponse confTerminalResponse) {
        // 将消息发送到 kafka
        Result result = new Result(Constants.CODE_PARAMETERS_ERROR);
        kafkaProducer.nodeActivateResponse(confTerminalResponse);
        result.setCode(Constants.CODE_SUCCESS);

        return result;
    }


    /**
     *  节点配置项设置 (服务--节点)
     *
     * @return
     */
    @RequestMapping(value = "/nodeItemSet", method = RequestMethod.POST)
    @ResponseBody
    public Result nodeItemSet(String itemData) {
        // 将消息发送到 kafka
        Result result = new Result(Constants.CODE_PARAMETERS_ERROR);
        JSONObject jsStr = JSONObject.parseObject(itemData);
        ConfItemSet confItemSet = JSONObject.toJavaObject(jsStr, ConfItemSet.class);
        kafkaProducer.nodeItemSet(itemData);
        result.setCode(Constants.CODE_SUCCESS);

        return result;
    }

    /**
     *  硬件信息上报响应 (服务--节点)
     *
     * @return
     */
    @RequestMapping(value = "/statusReportResponse", method = RequestMethod.POST)
    @ResponseBody
    public Result statusReportResponse(String statusReportData) {
        // 将消息发送到 kafka
        Result result = new Result(Constants.CODE_PARAMETERS_ERROR);
        kafkaProducer.statusReport(statusReportData);
        result.setCode(Constants.CODE_SUCCESS);

        return result;
    }



}
