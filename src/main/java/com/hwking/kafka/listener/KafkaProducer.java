package com.hwking.kafka.listener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hwking.kafka.bean.*;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Log
@Component
public class KafkaProducer {
    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${spring.kafka.topic}")
    private String topicOrder;

    /**
     * 发送test
     *
     * @param messageBean 消息实例
     */
    public void sendMessage(MessageBean messageBean) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将消息实例序列化为json格式的字符串
        String message = gson.toJson(messageBean);
        // 发送消息
        kafkaTemplate.send(topicOrder, message);
        // 打印消息
        log.info("发送 send message：" + message);
    }

    /**
     * 发送 节点激活消息  节点--服务  上行 (测试)
     *
     * @param confTerminal 消息实例
     */
    public void nodeActivate(ConfTerminal confTerminal) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将消息实例序列化为json格式的字符串
        String message = gson.toJson(confTerminal);
        // 发送消息
        kafkaTemplate.send("conf_up_channel", message);
        // 打印消息
        log.info("发送 send nodeActivate message：" + message);
    }

    /**
     * 发送 节点激活消息响应  服务--节点  下行
     *
     * @param confTerminalResponse 消息实例
     */
    public void nodeActivateResponse(ConfTerminalResponse confTerminalResponse) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将消息实例序列化为json格式的字符串
        String message = gson.toJson(confTerminalResponse);
        // 发送消息
        try {
            Thread.sleep(1000); //1000 毫秒，也就是1秒.
            kafkaTemplate.send("conf_down_channel", message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 打印消息
        log.info("发送 send nodeActivateResponse message：" + message);
    }

    /**
     * 发送 节点激活消息 确认  节点--服务  上行 (测试)
     *
     * @param confTerminalResponseConfirm 消息实例
     */
    public void nodeActivateResponseConfirm(ConfTerminalResponseConfirm confTerminalResponseConfirm) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将消息实例序列化为json格式的字符串
        String message = gson.toJson(confTerminalResponseConfirm);
        // 发送消息
        kafkaTemplate.send("conf_up_channel", message);
        // 打印消息
        log.info("发送 send nodeActivateResponseConfirm message：" + message);
    }



    /**
     *  发送 节点配置项设置  服务--节点  下行
     *
     * @param itemData
     */
    public void nodeItemSet (String itemData) {
        // 发送消息
        kafkaTemplate.send("conf_down_channel", itemData);
        // 打印消息
        log.info("发送 send nodeItemSet message：" + itemData);
    }

    /**
     *  发送 硬件信息响应  服务--节点  下行
     *
     * @param statusReportData
     */
    public void statusReport (String statusReportData) {
        // 发送消息
        kafkaTemplate.send("conf_down_channel", statusReportData);
        // 打印消息
        log.info("发送 send statusReport message：" + statusReportData);
    }
}
