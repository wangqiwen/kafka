package com.hwking.kafka.listener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hwking.kafka.bean.*;
import com.hwking.kafka.comm.Utils;
import com.hwking.kafka.service.RestService;
import lombok.extern.java.Log;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Log
@Component
public class KafkaConsumer {

    @Resource
    private RestService restService;

    @Resource
    private KafkaProducer kafkaProducer;


    /**
     * 收取 节点-服务 上行
     */
    @KafkaListener(topics = "conf_up_channel", containerFactory = "kafkaListenerContainerFactory")
    public void up(@Payload String message) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将接收到的消息反序列化消息实例
        ConfType confType = gson.fromJson(message,ConfType.class);
        // 打印消息
        log.info("收到上行 confType：" + confType.getOpt_type()+", message:"+message );

        if(confType.getOpt_type().equals("terminal_register")){  //激活
            // 将接收到的消息反序列化消息实例
            ConfTerminal confTerminal = gson.fromJson(message,ConfTerminal.class);
            //发送到平台
            restService.nodeActivate(confTerminal);
        }else if(confType.getOpt_type().equals("reg_resp_confirm")){ //确认
            // 将接收到的消息反序列化消息实例
            ConfTerminalResponseConfirm confTerminalResponseConfirm = gson.fromJson(message,ConfTerminalResponseConfirm.class);
            //发送到平台 确认激活
            restService.nodeActivateConfirm(confTerminalResponseConfirm);
        }else if(confType.getOpt_type().equals("conf_update_confirm")) { //配置修改确认
            // 将接收到的消息反序列化消息实例
            ConfItemSetResponse confItemSetResponse = gson.fromJson(message,ConfItemSetResponse.class);
            //发送到平台 修改反馈
            restService.nodeConfUpdateConfirm(confItemSetResponse);
        }else if(confType.getOpt_type().equals("hw_status_report")) { // 数据上报
            // 将接收到的消息反序列化消息实例c
            ConfServiceInfo confServiceInfo = gson.fromJson(message,ConfServiceInfo.class);
            //发送到平台 修改反馈
            restService.statusReport(message);
        }

    }

/*

    */
/**
     * 收取   服务--节点 下行 (模拟节点发送确认)
     *//*

    @KafkaListener(topics = "conf_down_channel", containerFactory = "kafkaListenerContainerFactory")
    public void down(@Payload String message) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.setDateFormat("yyyy-MM-dd HH:mm:ss");
        Gson gson = builder.create();
        // 将接收到的消息反序列化消息实例
        ConfType confType = gson.fromJson(message,ConfType.class);
        // 打印消息
        log.info("收到下行 confType：" + confType.getOpt_type()+", message:"+message );
        if(confType.getOpt_type().equals("reg_response")){
            // 将接收到的消息反序列化消息实例
            ConfTerminalResponse confTerminalResponse = gson.fromJson(message,ConfTerminalResponse.class);
            // 打印消息
            log.info("收到 nodeResponseConsume message：" + message);
        }else if(confType.getOpt_type().equals("conf_update")){
            log.info("收到 conf_update  message：" + message);

        }

    }
*/

}
